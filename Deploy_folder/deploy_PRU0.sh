#! /bin/bash
set -x  #Este comando faz com que alÃ©m do print dos comandos, seja mostrado todo o comando executado com um + para identificar o qual o comando e normal para o print.

echo "WARNING: EXECUTAR ESTE SCRIPT COMO SUDO!!! sudo ./deploy_PRU0"

##############################################################################
#
# UIO pruss
##############################################################################


# compiles the LOADER (only) and starts the PRU


#echo "*******************************************************"
#echo "To continue, press any key:"
#read

echo "removing old files"
	rm loaderPRU0
	rm *.bin

echo "compiling loader" 
	gcc -o loaderPRU0 loader_PRU0.c -lm -lprussdrv 

echo "-Placing the firmware"
	hexpru bin.cmd *.out
#PRU_CORE_USED=$((PRU_CORE+1)) #Dont change this unless you know
#	cp gen/*.out /lib/firmware/am335x-pru$PRU_CORE_USED-fw


echo "-Configuring Pins"
	#config-pin -a $HEADER$PIN_NUMBER pruout
	#config-pin -q $HEADER$PIN_NUMBER
	#to see a list of modes use:> config-pin -l P9_41
	config-pin -a P9_25 pruout #LED0
	echo "LED0 :" 
	config-pin -q P9_25
	echo " "
	config-pin -a P9_27 pruout #LED1
	echo "LED0:" 
	config-pin -q P9_27 
	echo " "
#CAREFUL HERE!!! begin
	####################  CAREFUL WITH THIS CONFIGURATION (The pins P9_41 (2 modes) and P9_17 are interconected - configure one as input and other as outoup. Never 2 as outputs)
	config-pin -a P9_41 gpio #just to disconect the virtual pin from P9_24 physical pin
	config-pin -a P9_91 pruin  #Button0 P9_91 = P9_41B - P9_41 reads R31 position 16. To read P9_41, use P9_91
	config-pin -a P9_17 pwm  #default- as input PWM sincronization pin (input)
	echo "Botão:" 
	config-pin -q P9_91
	echo " "
	
	
#My personal cape V0.2	interconect the pins P9_17 and P9_41(A and B) for easily way to debug. It also interconect the PINS P9_22 and P9_28

	###################
	#ADC control pins
	config-pin -a P9_22 pwm #(PWM OUTPUT) - if alterated. must alterate P9_28 too
	config-pin -a P9_28 pruin #CONVSTAB  INPUT PRU0 counter
	echo "Conversão request: (output on P9_22 and input P9_28)" 
	#config-pin -q P9_22
	config-pin -q P9_28 
	echo " " 
	
	
	
	

#Carefull here end#######################################################
	
 	config-pin -a P9_11 uart #Uart pin to ARM
	echo "GPS_TX pin to uart:" 
	config-pin -q P9_11
	echo " "	
  
	config-pin -a P9_31 pruout #OS2
	echo "Oversampling bit 2:" 
	config-pin -q P9_31
	echo " "
	config-pin -a P9_29 pruout #OS1
	echo "Oversampling bit 1:" 
	config-pin -q P9_29
	echo " "
	config-pin -a P9_30 pruout #OS0
	echo "Oversampling bit 0:" 
	config-pin -q P9_30
	echo " "
	config-pin -a P9_24 pruin #BUSY PRU_X
	config-pin -a P9_26 pruin #BUSY PRU_Y
	echo "BUSY pin input:" 
	config-pin -q P9_24
	echo " "
	
	
	#STDBY = P8_33 = GPIO0_[9]; (0x32 + 9) = GPIO9
	
	sleep 1
	
#NOTE!!!: to have expected effect of theses PWM activation scripts, you must 
#	run source ./deploy_PRU0.sh
#echo "configuring PWM module - carefull with this commands"	
#	echo 'BB-PWM0' >/sys/devices/platform/bone_capemgr/slots
#	echo | cat /sys/devices/platform/bone_capemgr/slots
#	#echo 'BB-PWM1' >/sys/devices/platform/bone_capemgr/slots
#	#echo 'BB-PWM2' >/sys/devices/platform/bone_capemgr/slots
#	#cd /sys/class/pwm/pwmchip6/
#	#echo 0 >/sys/class/pwm/pwmchip2/export
#	sleep 1
#	echo '0' >/sys/class/pwm/pwmchip0/export
#	echo 0 >/sys/class/pwm/pwmchip0/export
#	sleep 1
#  #echo 1 >export
#	#cd ./pwm0/
#	#echo 1 >/sys/class/pwm/pwmchip2/pwm0/enable
#	echo '1' >/sys/class/pwm/pwmchip0/pwm0/enable
#	echo 1 >/sys/class/pwm/pwmchip0/pwm0/enable  #cd /usr/PRU0/
#sudo ./configure_just_ePWM.sh				Movido as configurações do PWM para o programa em C....  Podem ser executadas separadamente.

	

echo "Removing the STAND-By (pin P8_33) mode" 
	echo 'out' > /sys/class/gpio/gpio9/direction  #changing to output pin
	echo '1' > /sys/class/gpio/gpio9/value  #changing this pin to high level
	echo " "
	
echo "Seting the RANGE  Pin (pin P8_38) to  mode (0 to +-5V, 1 to +-10V"  
	echo 'out' > /sys/class/gpio/gpio79/direction  #changing to output pin
	echo '1' > /sys/class/gpio/gpio79/value  #changing this pin to high level
	echo " "
	
	
	#DEBUG PART - Use this when independent mode on PRU- is enabled: P8_39 is GPIO76  #CS (active low) Chip select - Nem precisa uma vez que o padrão é zero.
#	echo "Seting the Chip select  Pin (pin P8_39) to zero. USE THIS ONLY IN INDEPENDENT RUN OF PRU0"   
#	echo 'out' > /sys/class/gpio/gpio76/direction  #changing to output pin
#	echo '0' > /sys/class/gpio/gpio76/value  #changing this pin to Low level (enable Chip AD chip select)
#	echo " "
	
	
#echo "Pulsing RESET pin P8_37" #P8_37 = GPIO2[14] = 2*32 + 14 = 78  
#echo 'out' > /sys/class/gpio/gpio78/direction  #changing to output pin
#echo '1' > /sys/class/gpio/gpio78/value  #changing this pin to high level

#echo "Reseting pin"
#echo '0' > /sys/class/gpio/gpio78/value  #changing this pin to Low level
#echo "Done."
	
	
	
	
echo "-Starting PRU"
	./loaderPRU0 text.bin data.bin
	
	
	
	
	
	
	
	
	
## Aqui estao as configs com o PWm funcionando e  alterando o dutycycle
#A unica coisa é que o reset do registrador está sendo feito no programa em C... 


#echo 'BB-PWM0' >/sys/devices/platform/bone_capemgr/slots
#echo 1 >/sys/class/pwm/pwmchip0/export

#cd /sys/class/pwm/pwmchip0/pwm0
#echo 10000000 > period		#gera uma frequencia de aprox 240 Hz
#echo 400000 > duty_cycle
#echo 1 > enable

#polaridade = normal
#Talvez escrever algo no uevent
#asyn = disabled
#cat control = auto
#...active_kids = 0
#...active_time = 0
#runtime_enabled = disabled
#runtime_status = unsuported
#...suspended_time = 0
#runtime_usage = 0




#PAra remover um slot (que vai acumulando)
#cd /sys/devices/platform/bone_capemgr# 
#cat slots
#5: P-O-L-   1 Override Board Name,00A0,Override Manuf,BB-PWM0
#6: P-O-L-   2 Override Board Name,00A0,Override Manuf,BB-PWM0
#7: P-O-L-   3 Override Board Name,00A0,Override Manuf,BB-PWM0
#8: P-O-L-   4 Override Board Name,00A0,Override Manuf,BB-PWM0
#root@beaglebone:/sys/devices/platform/bone_capemgr# echo -8 > /sys/devices/platform/bone_capemgr/slots
#root@beaglebone:/sys/devices/platform/bone_capemgr# echo -7 > /sys/devices/platform/bone_capemgr/slots
#root@beaglebone:/sys/devices/platform/bone_capemgr# echo -6 > /sys/devices/platform/bone_capemgr/slots
#root@beaglebone:/sys/devices/platform/bone_capemgr# echo -5 > /sys/devices/platform/bone_capemgr/slots

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	