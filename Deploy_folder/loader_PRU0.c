#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include "prussdrv.h"
#include "pruss_intc_mapping.h"
#include <string.h> //used on FIFO (memset part)

#define PRU_NUM 0			// define which pru is used
//#define SHM_OFFSET 2048		// http://www.embedded-things.com/bbb/understanding-bbb-pru-shared-memory-access/
#define SHM_OFFSET 0 // 0 is the first position of 12KB shared memory
#define C_PARAMETER_FILE "/usr/D-PMU_parameters/D-PMU_parameters.txt"

///////////////////////////////////////// Global Variables ///////////////////////////////////// 
uint16_t dpmu_pwm_freq_tbprd_reg; //Shared variable for PRU0 freq calculus
uint16_t dpmu_pwm_freq_amount_of_compensations;	//The amount of proposital delays over PWM counter increment.
uint16_t dpmu_pru0_led0_mode; //Parameter that change the PRU0 led behavior. 1 to exposes last sample (when toggle) and 0 to identify PRU0 running.
uint16_t dpmu_pru0_pwm_adjust_weight; //Variable that reflects the adjust weight for each PWM compensation
uint16_t dpmu_pru0_grid_nominal_freq; //Grid nominal frequency

//MEMORY ADDRESSES MAP
//Shared memory definitions - PRU0 can handle data from addr 50 to 99
#define PRU0_TBPRD_VALUE_ADDR 50		// Address to register that contains the PWM period register value (PWM freq)
#define PRU02LINUX_CTRL1_ADDR 51		//Receive the C_IDEAL_FREQUENCY set on PRU0 (amount samples per second)
#define PRU0_PWM_COMPENSATION_ADDR 52	//Addr of amount of PWM compensations (PWM fine adjust)
#define PRU0_LED0_MODE 53				//Addr of behavior of PRU0 cape LED. (Used to help the calibration) 
#define PRU0_PWM_COMPENSATION_WEIGHT 54 //Addr that stores the compensation delay to be applied over PWM.
#define LINUX2PRU0_NOMINAL_FREQ	56		//Send to PRU0 the configured system nominal frequency.

int pru_init(void)
{
	tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
	prussdrv_init();
	if (prussdrv_open(PRU_EVTOUT_0))
	{
		return -1;
	}
	else
	{
		prussdrv_pruintc_init(&pruss_intc_initdata);
		return 0;
	}
}

void pru_load(int pru_num, char* datafile, char* codefile)
{
	// load datafile in PRU memory
	prussdrv_load_datafile(pru_num, datafile);
	// load and execute codefile in PRU
	prussdrv_exec_program(pru_num, codefile);
}

void pru_stop(int pru_num)
{
	prussdrv_pru_disable(pru_num);
	prussdrv_exit();
}


#define SHM_OFFSET 0 // 0 is the first position of 12KB shared memory
volatile uint16_t* init_prumem()
{
	//volatile int32_t* p;
	volatile uint16_t* p;
	prussdrv_map_prumem(PRUSS0_SHARED_DATARAM, (void**)&p);
	return p+SHM_OFFSET;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 										Functions prototypes											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void decode_DPMU_file_parameters_pmu(void);

int main(void)
{
	char step_select = '0'; 
	int k=0;
	
	FILE *pwm0,*slot_overlay,*polarity,*enable; //Files that are used to write

	//Neste trecho estao as configuracoes do PWM da beaglebone para que a PRU consiga
	//	acessar e configurar pelos registradores da arquitetura. Estes passos eram
	//	executados atraves do script de deploy. Mas acorriam mais (comparado a agora)
	//	ocorrencias de instabilidades no inico de operacao. O PWM nao iniciava. 
	
	//Configuracao do PWM (em alto nível)
	//Comando base   -->   echo 'BB-PWM0' >/sys/devices/platform/bone_capemgr/slots
	slot_overlay = fopen("/sys/devices/platform/bone_capemgr/slots", "w");
    fseek(slot_overlay,0,SEEK_SET);
    fprintf(slot_overlay,"BB-PWM0");
    fflush(slot_overlay);
	fclose(slot_overlay);
	
	//Configuracao do PWM (em alto nível)
	//Comando base   -->   echo '0' >/sys/class/pwm/pwmchip0/export
	pwm0 = fopen("/sys/class/pwm/pwmchip0/export", "w");
    fseek(pwm0,0,SEEK_SET);
    fprintf(pwm0,"0");
    fflush(pwm0);
	fclose(pwm0);
	
	//Configuracao da polaridade (normal zero)  
	//Comando base    -->   echo 'inversed' >/sys/class/pwm/pwmchip0/pwm0/polarity
	polarity = fopen("/sys/class/pwm/pwmchip0/pwm0/polarity", "w");
    fseek(polarity,0,SEEK_SET);
    fprintf(polarity,"1");
    fflush(polarity);
	fclose(polarity);
	
	//Configuracao do enable
	//Comando base    -->  echo '1' >/sys/class/pwm/pwmchip0/pwm0/enable
	enable = fopen("/sys/class/pwm/pwmchip0/pwm0/enable", "w");
    fseek(enable,0,SEEK_SET);
    fprintf(enable,"1");
    fflush(enable);
	fclose(enable);
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Open and mount PRU1 parameters												//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	decode_DPMU_file_parameters_pmu();
	printf("TBPRD pwm freq register(%d)\n", dpmu_pwm_freq_tbprd_reg);
	printf("pwm amount of compensations(%d)\n", dpmu_pwm_freq_amount_of_compensations);
	
	printf("PRUSS driver init with return status: %i\n", pru_init());
	
	//Shared memory declaration. 
	volatile uint16_t* PRU0DataMem = init_prumem();
	
	PRU0DataMem[PRU0_TBPRD_VALUE_ADDR] = dpmu_pwm_freq_tbprd_reg;		//Sets for PRU0, the sampling frequency
	PRU0DataMem[PRU0_PWM_COMPENSATION_ADDR] = dpmu_pwm_freq_amount_of_compensations; //Sets for PRU0, the PWM adjust compensation.
	PRU0DataMem[PRU0_LED0_MODE] = dpmu_pru0_led0_mode; //Identify the PRU0 led mode.
	PRU0DataMem[PRU0_PWM_COMPENSATION_WEIGHT] = dpmu_pru0_pwm_adjust_weight; //compensation weight for each compensation over PWM.
	PRU0DataMem[LINUX2PRU0_NOMINAL_FREQ] = dpmu_pru0_grid_nominal_freq;
	
	//stopping old execution on pru
	// load the PRU code (consisting of both code and data bin file).
		// get the memory pointer to the shared data segment
//	volatile uint16_t* pruDataMem = init_prumem();
	//MEM data initialization
//	pruDataMem[ARM2PROG_PRU1_CONTROL1]=0xFFFF;
//	pruDataMem[PRU1_PROG_VERSION_ADDR] = 0XFFFF;
	sleep(1);
	puts("Loading Firmware on PRU");
	//loading pru 
 
	pru_load(PRU_NUM, "data.bin", "text.bin"); //CAREFUL WITH THE REESPECTIVE NAMES , vary from different setup

	sleep(1);
	printf("Ideal sampling frequency from shrdmemPRU0 addr 51 is  %d\n", (uint16_t)PRU0DataMem[PRU02LINUX_CTRL1_ADDR]);

	/*while(1){
		

	};
	pru_stop(PRU_NUM); */

	return 0;
};

/***************************************************************************************************
* Function to read and decode PRU1 parameters from PARAMETER file.
*
*
***************************************************************************************************/	
void decode_DPMU_file_parameters_pmu(void)
{

	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	size_t length, length_header;

	//Open file
	fp = fopen(C_PARAMETER_FILE, "r");
	//check file health
	if (fp == NULL)
	{
		printf("Error on PRU1 deploy. The Parameters file is empty.");
		exit(EXIT_FAILURE);
	}
	
	puts("--------------------Decoding process starts--------------------------------");

	//structure reset.
	//memset(&interpreted_data, 0x00, sizeof(interpreted_data));

	//Read and decode important lines (with '*' identifier )
	while ((length = getline(&line, &len, fp)) != -1) //seek file until it ends
	{
		if(line[0] == '*') //parameter identifier
		{
			if(strncmp(line,"*PWM_FREQ_TBPRD_EPWM",20) == 0)
			{
				length = getline(&line, &len, fp); //of the PWM period register.
				printf("pwm period: %s\r\n", line);
				dpmu_pwm_freq_tbprd_reg = atoi(line);
			}
			else if(strncmp(line,"*D-PMU_PWM_COUNTER_COMPENSATIONS",32) == 0)
			{
				length = getline(&line, &len, fp); //of the PWM compensation frequency (PWM adjust)
				printf("pwm adjust is: %s\r\n", line);
				dpmu_pwm_freq_amount_of_compensations = atoi(line); //Send the desired number of compensatios
			}
			else if(strncmp(line,"*D-PMU_PWM_COMPENSATION_WEIGHT",30) == 0)
			{
				length = getline(&line, &len, fp); //of the PWM compensation frequency (PWM adjust)
				printf("pwm adjust WEIGHT is: %s\r\n", line);
				dpmu_pru0_pwm_adjust_weight = atoi(line);
			}
			else if (strncmp(line,"*D-PMU_PWM_IDENTIFY_LAST_SAMPLE",31) == 0)
			{
				dpmu_pru0_led0_mode = 0;	//Normal mode, LED0 identifies the PRU0 running 
				length = getline(&line, &len, fp);
				if (strncmp(line,"yes",3) == 0)
				{
					dpmu_pru0_led0_mode = 1; //Calibration mode, LED0 identifies the last sample
				}
			}
			else if (strncmp(line,"*NOMINAL_SYS_FREQUENCY",22) == 0)
			{
				length = getline(&line, &len, fp);
				dpmu_pru0_grid_nominal_freq = atoi(line);
			}
			
			
			else
			{

			}
		}

	}
	puts("--------------------Decoding process finished--------------------------------");

	fclose(fp);
	//if (line)
	//	free(line);
	//exit(EXIT_SUCCESS);

}
