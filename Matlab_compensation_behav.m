close all;

ideal_freq = 3840;

compensation_amunt = 30;


%% Compensation logic over 1 second

compensation_ratio_threshold = ideal_freq / compensation_amunt;
compensated_counter = 1;
non_compensated_counter = 1;
compensation_behav = zeros(1,ideal_freq);

for i = 1:1:ideal_freq
    compensation_behav(i) = 0;
    if ( ( non_compensated_counter/compensated_counter ) >  compensation_ratio_threshold )
        compensation_behav(i) = 1;
        compensated_counter = compensated_counter+1;
        
    end
    non_compensated_counter = non_compensated_counter+1;
end

%% Result analisys
plot(compensation_behav)

fprintf('The number of compensated points should be %d \r\n', (compensated_counter-1));

