/*
 * This program is used to generate the ADC pulses - PRU0
 *
 * mainPRU0.c
 *
 * Version control:
 *
 * Vr  		/	Date	/ Observation
 * 0.1		/ 18-05-18	/ Initial version just with LED/button debug
 * 0.2		/ 08-06-18  / Migrated to drive UIO. Initial version on UIO
 * 0.3		/ 12-06-18  / Changed the FS and add the continuous mode (when C_CONVERSIONS_AMOUNT_LIMIT = 1111111)
 * 0.4		/ 17-06-18	/ Changed to PWM module control program.. Its not necessary more the delay to pin control.
 * 0.5      / 19-06-18  / Changed to Convstab to input pin.. to be analysed by PRU0 and wait Busy signal.
 * 0.6      / 27-06-18  / Implemented the CONVSTAB input to counter the number of requests until the expected FS be arrived, ALSO USING led RED AS output pulse
 * 0.7		/ 28-06-18  / Started the implementation of dynamic PWM frequency config
 * 0.8		/ 21-07-18  / Trying to remove the the Syncin register reset of PWM. to change sec detect (based on CONVSTAB) (don't write on PWM during it's operation)
 * 0.9		/ 30-07-18  / Changed the compensation method from counter compensation tu period compensation (dont change counter behaviour)
 * 1.0		/ 28-11-18  / addecquate to D-PMU cape v2
 * 2.0		/ 16-12-18  / Improved for performance. Less clock cicles usage.
 * 2.1		/ 07-02-19	/ Working above compensations dynamic increment, based on PRU0 button
 * 2.2		/ 24-07-19	/ Didnt start the dynamic frequency compensations. Now working on shared memory.
 * 2.3		/ 27-05-20	/ Changed architecture to FSM based. Verified the exact desired amount compensations per second on the logic analyzer
 * 2.4		/ 24-11-20	/ Adding 50 system monitoring capability.
 *
 *
 * PINs and R30 and R31 positions FOR PRU0 (ZER0)
 * PortPin  /   R30 (1<<X)  /	mapped to	/     R31 (1<<X) / mapped to
 * P8_11    /   15     		/    			/		none	/
 * P8_12    /   14      	/    			/		none	/
 * P8_15    /   none    	/    			/		15		/
 * P8_16    /   none    	/    			/		14		/
 * P9_24	/	none		/  		 		/		16		/  inputBUSYsig
 * P9_25	/	7			/ outputLED0    /		7		/
 * P9_27	/   5			/ outputLED1 	/		5		/
 * P9_28	/	3			/ 			    /		3		/ input CONVSTAB
 * P9_29	/	1			/	outOS1		/		1		/
 * P9_30	/	2			/	outOS0		/		2		/
 * P9_31	/	0			/ 	outOS2		/		0		/
 * P9_41A	/	none		/				/		16 (TAMBEM)/
 * P9_41B(set P9_91 (+50)) 	/	6			/  				/		6		/input button
 * P9_42B(set P9_92 (+50))	/	4			/				/		4		/
 *
 */
/************************************************************
* STANDARD BITS
************************************************************/
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <pru_cfg.h>
#include <sys_pwmss.h>
#include "registers.h"
#include "defines.h"

///////////////////////////////////DEFINES FOR THIS PROGRAM//////////////////////////////////////////////

#define C_IDEAL_FREQUENCY_SIXTY 3840.0 //Ideal frequency parameter - be used as CONVSTAB counter limit parameter.
#define C_IDEAL_FREQUENCY_FIFTY 3200.0 //Ideal frequency parameter - be used as CONVSTAB counter limit parameter.
#define C_INPUT_BUTTON0 BIT6 //6 //P9_41B same as P9_91 for input
#define C_OUTPUT_LED0 BIT7 //7 //P9_25 for output LED0
#define C_OUTPUT_LED1_1PPS BIT5 //5 //P9_27 for output LED1
#define C_CONVSTAB_INPUT BIT3 //3 //P9_28 for CONVSTAB signal
#define C_BUSY_INPUT BIT16 //16 //P9_24 for BUSY signal
#define C_OS0_OUTPUT BIT2 //2 //P9_30 Oversampling bit 0
#define C_OS1_OUTPUT BIT1 //1 //P9_29 Oversampling bit 0
#define C_OS2_OUTPUT BIT0 //0 //P9_31 Oversampling bit 0
#define C_CONVREQ_PULSE_WIDTH 400 //200 for 2 us, 100 for 1us, 400 for 4us ...

#define C_INDEPENDENT_MODE 0 // Independent mode flag (1 to run alone, 0 to run with PRU1)
//Acho que este par�metro deve vir da mem�ria compartilhada da PRU1

//Shared memory definitions - PRU0 can handle data from addr 50 to 99
#define PRU0_TBPRD_VALUE_ADDR 50		//Address to store and get the PWM Period register (TBPRD)
#define PRU02LINUX_CTRL1_ADDR 51		//Address to store the C_IDEAL_FREQUENCY_SIXTY set on PRU0
#define PRU0_PWM_COMPENSATION 52		//Amount of PWM compensations
#define PRU0_LED0_MODE 53				//Addr of behavior of PRU0 cape LED. (Used to help the calibration)
#define PRU0_PWM_COMPENSATION_WEIGHT 54 //Addr that stores the compensation delay to be applied over PWM.
#define PRU1_TO_PRU0_SYNC_CNTR 55		//Counter that is incremented always an 1 PPS sync occurs
#define LINUX2PRU0_NOMINAL_FREQ 56		//Grid nominal frequency
#define LINUX2PRU0_CTRL7 57				//
#define LINUX2PRU0_CTRL8 58				//
#define LINUX2PRU0_CTRL9 59				//

//////////////////////////////////MACROS to save cycles ////////////////////////////////////////////////
#define COMPARE_VAL(a, b) (a == b ? 1 : 0)//MUST BE TESTED  //Compare macro
#define ADD(p, n, m) (p = n + m)  //Add 2 values
#define ADD_ONE(a,r) (r = a + 1) //Add one to an value
#define LOAD_VAL(p, n) (p = n) //Loads the value to an specific  variable
#define DIV_VALUE(n, m, t) ((n / m) > t ? 1:0) //Divide 2 values
#define SET_BITV2(p, n) (p |= n) //set the bit N
#define CLR_BITV2(p, n) (p &= ~n) // clear the bit N
#define TGL_BITV2(p, n) (p ^= n)  //Toggle the specified bit n

/* Non-CT register defines */
#define CM_PER_EPWMSS0 (*((volatile unsigned int *)0x44E000CC))

/* Mapping Constant table register to variable */
volatile pruCfg CT_CFG __attribute__((cregister("CFG", near), peripheral));

//Nao sei o que faz, mas permite eu setar agora o reg SYSCONFIG do PWMSS0
 volatile sysPwmss PWMSS0 __attribute__((cregister("PWMSS0", near), peripheral));

//Volatile delaration of the shared ram
 volatile uint16_t* shared_ram_config_PRUSS;

//GPIO parameters
volatile register unsigned int __R31, __R30;  //R31 =  INPUT; R30 = Output

/* Prototypes */
//void init_epwm( uint16_t TBPRD, uint16_t TBCNT, uint16_t CMPA );
void init_epwm(int counter);
void handle_1pps_led(int frequency_counter, float grid_frequency);
void update_epwm_freq(int frequency);

uint8_t convreq_actual_state, convreq_last_state;
uint8_t busy_last_sate, busy_actual_state;
uint16_t epwm_tbprd_val = 23000;
uint16_t dpmu_pru0_mode = 0;	//0-LED0 identify pru0 running, 1-identify LED0 pru0 last sample request

void main(void) {

	//Shared Memory creation. Commom for both PRUSS
	shared_ram_config_PRUSS = (volatile uint16_t *)0x10000;  //memory shared (ARM-PRUs/PRUs-PRUs) - Its not possible to write more than address 100.
	int frequency_counter = 0;
	int counter_compensated = 1;
	int counter_not_compensated = 1;
	float compensation_ratio = 0;
	float frequency_sampling = 0;
	//int debug_counter = 0;
	int compensated_flag_TBPRD = 0; //this flag is used to reset detect an compensation and return the TBPRD register value to the original calculated value. 0 = not compensated, 1 = already compensated.
	uint16_t dpmu_pwm_freq_amount_of_compensations = 2450;
	uint16_t dpmu_pwm_compensation_weight = 1; //compensation weight. 1 = 10 ns, 2 = 20 ns for each compensation.
	t_fsm_sampling_state fsm_state;
	uint16_t prev_sync_countr;


	//Reset for Leds status
	CLR_BITV2(__R30,C_OUTPUT_LED0);
	CLR_BITV2(__R30,C_OUTPUT_LED1_1PPS);
	// Oversampling configuration:
	CLR_BITV2(__R30,C_OS0_OUTPUT);
	CLR_BITV2(__R30,C_OS1_OUTPUT);
	CLR_BITV2(__R30,C_OS2_OUTPUT);

	//Remove Standby from communication bus
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 									Shared memory read values											//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//obtain the system nominal frequency
	if (shared_ram_config_PRUSS[LINUX2PRU0_NOMINAL_FREQ] == 60)
	{
		shared_ram_config_PRUSS[PRU02LINUX_CTRL1_ADDR] = (uint16_t)C_IDEAL_FREQUENCY_SIXTY; //initial value to status.
		frequency_sampling = (float)C_IDEAL_FREQUENCY_SIXTY;
	}
	else
	{
		shared_ram_config_PRUSS[PRU02LINUX_CTRL1_ADDR] = (uint16_t)C_IDEAL_FREQUENCY_FIFTY; //initial value to status.
		frequency_sampling = (float)C_IDEAL_FREQUENCY_FIFTY;
	}
	epwm_tbprd_val = shared_ram_config_PRUSS[PRU0_TBPRD_VALUE_ADDR];
	dpmu_pwm_freq_amount_of_compensations = (uint16_t)shared_ram_config_PRUSS[PRU0_PWM_COMPENSATION];
	dpmu_pru0_mode = shared_ram_config_PRUSS[PRU0_LED0_MODE];
	dpmu_pwm_compensation_weight = shared_ram_config_PRUSS[PRU0_PWM_COMPENSATION_WEIGHT];

	//update compensation ratio value
	compensation_ratio = ((float)(frequency_sampling) / (float)(dpmu_pwm_freq_amount_of_compensations));

	fsm_state = FSM_INIT;
	convreq_last_state = LOW;
	busy_last_sate = LOW;
	SET_BITV2(__R30,C_OUTPUT_LED0);

	prev_sync_countr = shared_ram_config_PRUSS[PRU1_TO_PRU0_SYNC_CNTR];

    while (1)
    {

    	//Update input pin status
    	busy_actual_state = LOW;
    	if (__R31 & C_BUSY_INPUT)
    	{
    		busy_actual_state = HIGH;
    	}

    	convreq_actual_state = LOW;
    	if (__R31 & C_CONVSTAB_INPUT)
    	{
    		convreq_actual_state = HIGH;
    	}

    	switch (fsm_state)
    	{
			case FSM_INIT:
			{
				//Pin values initial values
				convreq_actual_state = LOW;
				busy_actual_state = LOW;

				//PWM Initialization
				init_epwm(epwm_tbprd_val);

				//SYSCONFIG_bit register page 1964 on datasheet
				PWMSS0.SYSCONFIG_bit.SOFTRESET = 0x1;
				PWMSS0.SYSCONFIG_bit.IDLEMODE = 0x1; //No iddle mode.
				PWMSS0.SYSCONFIG_bit.FREEEMU = 0x1; //not sensitive to emulation (debug) signal
				PWMSS0.SYSCONFIG_bit.STANDBYMODE = 0x0;
				CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

				while(((PWMSS0.EPWM_TBSTS & 0x0002) == 0x0002))
				{
					if (C_INDEPENDENT_MODE == 1)
					{
						PWMSS0.EPWM_TBSTS = 0x0002; //When PRU1 isnt used, the PRU0 should also reset the Sync register flag
					}
				}//SYNC not set (only PRU1 can write in this register)

				//Define next state
				if (convreq_actual_state == 1 & convreq_last_state == 0) //low to High
				{
					fsm_state = FSM_CONVERSION_REQ;
				}
				else
				{
					fsm_state = FSM_WAIT_REQ;
				}

			}
			break;

			case FSM_WAIT_REQ:
			{

				//Define next state
				if (convreq_actual_state == 1 & convreq_last_state == 0) //low to High
				{
					fsm_state = FSM_CONVERSION_REQ;
				}

			}
			break;

			case FSM_CONVERSION_REQ: //New sample has been requested
			{
				//verify if it is possible to  return to the original frequency
				if(compensated_flag_TBPRD == 2)
				{
					compensated_flag_TBPRD = 0;
					update_epwm_freq(epwm_tbprd_val);
				}
				else
				{
					compensated_flag_TBPRD = compensated_flag_TBPRD * 2; //gives 1 conversion delay between set flag and clear flag
				}

				//Increment the sample counter
	    		frequency_counter++;

				//Compensation calculus
				if (((float)(counter_not_compensated / (float)counter_compensated )) >= compensation_ratio)
				{
					ADD_ONE(counter_compensated, counter_compensated);
					//set flag
					compensated_flag_TBPRD = 1;

					//Slow the fs
					update_epwm_freq(epwm_tbprd_val + dpmu_pwm_compensation_weight);

				   //init_epwm(25001);
					TGL_BITV2(__R30,C_OUTPUT_LED0);
				}

				ADD_ONE(counter_not_compensated, counter_not_compensated);

				if (busy_actual_state == 0) //After a convreq, busy rise automatically. At this point if busy is low, the ADC is already free for another conversion
				{
					fsm_state = FSM_WAIT_REQ;
				}
				else
				{
					fsm_state = FSM_WAIT_BUSY; //Busy still high, should wait busy goes down
				}
			}
			break;

			case FSM_WAIT_BUSY:
			{
				//Define next state
				if (busy_actual_state == 0 & busy_last_sate == 1) //Conversion end and ready to be read
				{
					fsm_state = FSM_WAIT_REQ;
				}
			}
			break;

			default:
				fsm_state = FSM_INIT ;

    	}

    	//Reset sync and counter here
		if( ((PWMSS0.EPWM_TBSTS & 0x0002) == 0x0002) || (prev_sync_countr != shared_ram_config_PRUSS[PRU1_TO_PRU0_SYNC_CNTR]) ) //TODO: Change this to address of shared memory (set by PRU1)
		{
			//Reset of counters - new second
			frequency_counter = 0;
			counter_compensated = 1;
			counter_not_compensated = 1;
			fsm_state = FSM_CONVERSION_REQ;

			if (C_INDEPENDENT_MODE == 1)
			{
				PWMSS0.EPWM_TBSTS = 0x0002;  //When PRU1 is not used, the PRU0 reset the Sync register flag
			}
			prev_sync_countr = shared_ram_config_PRUSS[PRU1_TO_PRU0_SYNC_CNTR]; //Update teh sync counter with the last shared memory value.
		}

		//update last state registers
		convreq_last_state = convreq_actual_state;
		busy_last_sate = busy_actual_state;

		//debug
		if (frequency_counter >= (frequency_sampling-10))	//Last 10 samples
		{
			SET_BITV2(__R30,C_OUTPUT_LED1_1PPS);
		}
		if(frequency_counter == 1)
		{
			CLR_BITV2(__R30,C_OUTPUT_LED1_1PPS);

		}
		//Set or clear 1PPS LED
//		handle_1pps_led(frequency_counter);

    } //while 1
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* Brief: 	void handle_1pps_led(int frequency_counter);
 * Brief:	This function is used set handle 1PPS Led.
 * Param:	frequency_counter: The amount of conversions already requested
 * 			grid_frequency: grid nominal_frequency
 * Output:	None
 */
void handle_1pps_led(int frequency_counter, float grid_frequency)
{
	//Set 1PPS Led
	if (frequency_counter < round(grid_frequency/2))
	{
		SET_BITV2(__R30,C_OUTPUT_LED1_1PPS);
	}
	else
	{
		CLR_BITV2(__R30,C_OUTPUT_LED1_1PPS);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///configured ti work with frequency_sampling samples per second.
//LOW resolution mode:
/*
 * Init ePWM
 */
void init_epwm(int initial_val)
{


    /* Enable PWMSS1 clock signal generation */
    while (!(CM_PER_EPWMSS0 & 0x2))
    {
        CM_PER_EPWMSS0 |= 0x2;
    }
    /*CM_PER_EPWMSS0 BITS:
			31-18 Reserved R 0h
			17-16 IDLEST R 3h Module idle status.
					0 = Func : Module is fully functional, including OCP
					1 = Trans : Module is performing transition: wakeup, or sleep, or
					sleep abortion
					2 = Idle : Module is in Idle mode (only OCP part). It is functional if
					using separate functional clock
					3 = Disable : Module is disabled and cannot be accessed
			15-2 Reserved R 0h
			1-0 MODULEMODE R/W 0h Control the way mandatory clocks are managed.
					0 = DISABLED : Module is disable by SW. Any OCP access to
					module results in an error, except if resulting from a module wakeup
					(asynchronous wakeup).
					1 = RESERVED_1 : Reserved
					2 = ENABLE : Module is explicitly enabled. Interface clock (if not
					used for functions) may be gated according to the clock domain
					state. Functional clocks are guarantied to stay present. As long as in
					this configuration, power domai
    */



    //PWMSS0.EPWM_TBCTL = 0x0034; //100MHz - sync_in - up - with shadow logic
    PWMSS0.EPWM_TBCTL = 0x003C; //100MHz - sync_in - up - with shadow logic
    //PWMSS0.EPWM_TBCTL = 0x0434;  //2  50MHz - sync_in - up
    //PWMSS0.EPWM_TBCTL = 0x834;  //4 25MHz - sync_in - up
    //PWMSS0.EPWM_TBCTL = 0x1034; //8 12.5MHz - sync_in - up
    /*
     * � If TBCTL[PRDLD] = 0, then the shadow is enabled and any write on TBPRD or read will automatically go to the
		shadow register. In this case, the active register will be loaded from the shadow register when the
		time-base counter equals zero.
		� If TBCTL[PRDLD] = 1, then the shadow is disabled and any write or read will go directly to the active
		register, that is the register actively controlling the hardware.
		� The active and shadow registers share the same memory map address
     *
     */

    //Enabling the PWM, the register only load the configuration if the clock is enabled
    PWMSS0.CLKCONFIG = 0x0100;

    //These bits determine the period of the time-base counter. This sets the PWM frequency
    PWMSS0.EPWM_TBPRD = initial_val;

    //PWMSS0.EPWM_CMPCTL = 0x0000; 								//shadownmode = true on CMPA and CMPB registers
    PWMSS0.EPWM_CMPCTL = 0x0050;								//immediate mode to CMPA and CMPB registers

    PWMSS0.EPWM_AQCTLA = 0x0024; 								//define CMPA as high level and TBPRD as low level

    PWMSS0.EPWM_TBCNT = initial_val - 40;						//Set supposing that an sync has occured, not from zero.


	PWMSS0.EPWM_CMPA = initial_val - C_CONVREQ_PULSE_WIDTH;		//-40; //configure the duty cycle A - to frequency_sampling
	//PWMSS0.EPWM_CMPA = 6444; 									//configure the duty cycle A - to 256 pts
	//PWMSS0.EPWM_CMPA = 12980; 								//configure the duty cycle A - to 7680 hz -64 pts

	PWMSS0.EPWM_TBPHS = initial_val - C_CONVREQ_PULSE_WIDTH;	//move to the counter that set conversion request

    PWMSS0.EPWM_CMPB = 0; 										//configure the duty cycle A

    if (dpmu_pru0_mode == 0)
    {
    	SET_BITV2(__R30,C_OUTPUT_LED0);								//Debug LED to identify the PR1 Working status
    }
}

/*
 * Update ePWM frequency
 */
void update_epwm_freq(int frequency)
{
    PWMSS0.EPWM_TBPRD = frequency;
}
